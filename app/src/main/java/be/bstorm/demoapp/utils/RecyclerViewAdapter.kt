package be.bstorm.demoapp.utils

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import be.bstorm.demoapp.R
import be.bstorm.demoapp.api.pokemon.PokemonListItem

class PokemonRecyclerViewHolder(val view: View): RecyclerView.ViewHolder(view) {
    val name: TextView by lazy {
        view.findViewById(R.id.tvPokemonName)
    }
}

class PokemonRecyclerViewAdapter(
    val layout: Int,
    var items: List<PokemonListItem>,
    val onClick: (PokemonListItem) -> Unit
): RecyclerView.Adapter<PokemonRecyclerViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonRecyclerViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layout, parent, false)

        Log.d("PokemonRecyclerViewAdapter", "onCreateViewHolder: $view")


        return PokemonRecyclerViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: PokemonRecyclerViewHolder, position: Int) {
        val item = items[position]
        Log.d("PokemonRecyclerViewAdapter", "onBindViewHolder: $item")

        holder.name.text = item.name

        holder.view.setOnClickListener {
            onClick(item)
        }
    }
}