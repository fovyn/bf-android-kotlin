package be.bstorm.demoapp.ui.main

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import be.bstorm.demoapp.DemoApplication
import be.bstorm.demoapp.R
import be.bstorm.demoapp.api.pokemon.PokemonListItem
import be.bstorm.demoapp.dal.DbHelper
import be.bstorm.demoapp.dal.entities.UserEntity
import be.bstorm.demoapp.dal.scopes.RoomScope
import be.bstorm.demoapp.databinding.ActivityMainBinding
import be.bstorm.demoapp.utils.PokemonRecyclerViewAdapter
import kotlinx.coroutines.launch
import java.time.LocalDate

class MainActivity : AppCompatActivity() {
    private val viewModel: PokemonViewModel by viewModels()
    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }
    private val app: DemoApplication by lazy {
        application as DemoApplication
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        app.log(Log.WARN, "MainActivity", "OnCreate")

        binding.rvPokemons.layoutManager = GridLayoutManager(this, 2)
        binding.rvPokemons.adapter = PokemonRecyclerViewAdapter(R.layout.recyclerview_pokemon_list_item, listOf<PokemonListItem>()) {
            Log.d("MainActivity", "onCreate: ${it.url.split("/").last()}")
            viewModel.getPokemon(it.url.split("/").last())
        }


        viewModel.pokemonList.observe(this) {
            Log.d("MainActivity", "onCreate: $it")
            binding.rvPokemons.adapter = PokemonRecyclerViewAdapter(R.layout.recyclerview_pokemon_list_item, it) {
                Log.d("MainActivity", "onCreate: ${it.url}")
                val splitted = it.url.split("/")


                viewModel.getPokemon(splitted[splitted.size - 2])
            }
            binding.rvPokemons.adapter!!.notifyDataSetChanged()
        }
        viewModel.loadAll()

        binding.btnAdd.setOnClickListener {
            add()
        }

        RoomScope().launch { DbHelper.instance(this@MainActivity).userDao().insert(UserEntity("blop2", "blop", LocalDate.now(), LocalDate.now())) }

        RoomScope().launch {
            DbHelper.instance(this@MainActivity).userDao().findAll().collect {
                Log.d("MainActivity", "onCreate: $it")
            }
        }
    }

    fun add() {
        Log.d("MainActivity", "onResume")
        viewModel.addPokemon(PokemonListItem("Blop", "http://blop"))
    }
}