package be.bstorm.demoapp.ui.main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import be.bstorm.demoapp.DemoApplication
import be.bstorm.demoapp.api.pokemon.PokemonListItem
import be.bstorm.demoapp.api.pokemon.PokemonListResponse
import be.bstorm.demoapp.api.pokemon.PokemonResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PokemonViewModel(): ViewModel() {
    val pokemonList = MutableLiveData<List<PokemonListItem>>()

    fun loadAll() {
        DemoApplication.pokeApi.getPokemons().enqueue(object : Callback<PokemonListResponse> {
            override fun onResponse(
                call: Call<PokemonListResponse>,
                response: Response<PokemonListResponse>
            ) {
                response.body()?.let {
                    pokemonList.value = it.results
                }
            }

            override fun onFailure(call: Call<PokemonListResponse>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    fun addPokemon(pokemon: PokemonListItem) {
        pokemonList.value = pokemonList.value?.plus(pokemon)
    }

    fun getPokemon(id: String) {
        DemoApplication.pokeApi.getPokemonById(id).enqueue(object : Callback<PokemonResponse> {
            override fun onResponse(
                call: Call<PokemonResponse>,
                response: Response<PokemonResponse>
            ) {
                Log.d("POKEMON", response.body().toString())
            }

            override fun onFailure(call: Call<PokemonResponse>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }
}