package be.bstorm.demoapp

import android.app.Application
import android.util.Log
import be.bstorm.demoapp.api.pokemon.PokeApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DemoApplication: Application() {
    val appName: String = "DemoApp"

    companion object {
        const val MIN_LOG_LEVEL = Log.INFO

        val apiBuiler = Retrofit.Builder()
            .baseUrl("https://pokeapi.co/api/v2/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val pokeApi by lazy { apiBuiler.create(PokeApi::class.java) }
    }

    fun log(lvl: Int, context: String, msg: String) {
        if (lvl >= MIN_LOG_LEVEL) {
            Log.println(lvl, context, msg)
        }
    }
}