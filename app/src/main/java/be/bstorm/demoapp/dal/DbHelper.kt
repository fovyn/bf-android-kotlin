package be.bstorm.demoapp.dal

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import be.bstorm.demoapp.dal.daos.UserDao
import be.bstorm.demoapp.dal.entities.UserEntity

@Database(entities = [UserEntity::class], version = 1, exportSchema = false)
abstract class DbHelper: RoomDatabase() {
    abstract fun userDao(): UserDao

    companion object {
        const val DB_NAME = "demo.db"

        @Volatile
        private var instance: DbHelper? = null
        fun instance(context: Context): DbHelper {
            if (instance == null) {
                instance = Room
                    .databaseBuilder(context, DbHelper::class.java, DB_NAME)
                    .fallbackToDestructiveMigration()
                    .build()
            }

            return instance!!
        }
    }

}