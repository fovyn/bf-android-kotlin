package be.bstorm.demoapp.dal.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import be.bstorm.demoapp.dal.entities.UserEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {
    @Query("SELECT * FROM user")
    fun findAll(): Flow<List<UserEntity>>

    @Query("SELECT * FROM user WHERE id = :id")
    fun findById(id: Long): Flow<UserEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(obj: UserEntity)
}