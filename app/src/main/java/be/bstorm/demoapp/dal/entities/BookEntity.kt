package be.bstorm.demoapp.dal.entities

import androidx.room.Entity

@Entity(tableName= "books", primaryKeys = ["id"])
data class BookEntity(val id: Int, val name: String) {
}