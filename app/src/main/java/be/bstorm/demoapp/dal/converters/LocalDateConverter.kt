package be.bstorm.demoapp.dal.converters

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.room.TypeConverter
import java.time.LocalDate

class LocalDateConverter {

    @RequiresApi(Build.VERSION_CODES.O)
    @TypeConverter
    fun Long.toLocalDate(): LocalDate {
        return LocalDate.ofEpochDay(this)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @TypeConverter
    fun LocalDate.toLong(): Long {
        return this.toEpochDay()
    }
}