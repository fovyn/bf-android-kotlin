package be.bstorm.demoapp.dal.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import be.bstorm.demoapp.dal.converters.LocalDateConverter
import java.time.LocalDate

@Entity(tableName = "user")
@TypeConverters(LocalDateConverter::class)
data class UserEntity(
    var username: String,
    var password: String,
    var createdAt: LocalDate,
    var updatedAt: LocalDate,
    var active: Boolean = true,
    @PrimaryKey(autoGenerate = true) var id: Long? = null,
)
