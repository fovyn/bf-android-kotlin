package be.bstorm.demoapp.api.pokemon

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface PokeApi {
    @GET("pokemon")
    fun getPokemons(): Call<PokemonListResponse>

    @GET("pokemon/{id}/")
    fun getPokemonById(@Path("id") id: String): Call<PokemonResponse>
}