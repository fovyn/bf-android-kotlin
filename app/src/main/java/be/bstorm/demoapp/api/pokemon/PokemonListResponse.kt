package be.bstorm.demoapp.api.pokemon

data class PokemonListItem(val name: String, val url: String)


data class PokemonListResponse(
    val count: Int,
    val next: String?,
    val previous: String?,
    val results: List<PokemonListItem>
)

data class PokemonResponse(val name: String, val weight: Float)
